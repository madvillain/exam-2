# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Employee.delete_all

1000.times.each do |n|
  array = %W[Clint Joe Bill Bob Hank Dale Daisy Meg Nancy]
  employees = Employee.create ([{rate: rand(30), name: array.shuffle[0]}])


end